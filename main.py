from api import servers, games
from sanic import Sanic
from sanic_cors import CORS, cross_origin
from sanic_openapi import swagger_blueprint, doc
from sanic.response import json, text, redirect
from sanic.exceptions import ServerError
from services import kube, tokens
from services.limits import allowed_client
from tests import k8s

#############################
###        INIT APP       ###
#############################
app = Sanic(name="GaaS-API")
app.blueprint(swagger_blueprint)
app.config["API_VERSION"] = "0.1"
app.config["API_TITLE"] = "Gaming-as-a-Service OpenAPI"
app.config["API_DESCRIPTION"] = "API for orchestrating GameServers deployed in Kubernetes"
app.config["API_CONTACT_EMAIL"] = "info@lejetlan.dk"

app.config['CORS_AUTOMATIC_OPTIONS'] = True
CORS(app, resources={r"/*": {"origins": "*"}})

kube.init()

# Service that handles steam game server tokens
TokenHandler = tokens.SteamToken()

#############################
###         ROUTES        ###
#############################
ALLOWED_CLIENTS = ["10.0.0.0/8", "172.20.11.0/24", "172.17.0.0/16", "127.0.0.1/32", "172.19.0.0/24"] # DH and local Docker


@app.get("/")
@doc.exclude(True)
async def home():
    return redirect('/swagger')


@app.get("/api/servers")
@doc.tag("servers")
@doc.description("Get a list of gameserver deployed in kubernetes")
async def getServerList(request):
    try:
        return json(servers.getServerList(request.ip))
    except Exception as e:
        raise ServerError(e)


@app.get("/api/servers/<id>")
@doc.tag("servers")
@doc.description("Get info of a specific gameserver deployed in kubernetes")
async def getServerInfo(request, id):
    try:
        return json(servers.getServerInfo(id, request.ip))
    except Exception as e:
        raise ServerError(e)


@app.post("/api/servers/add")
@doc.tag("servers")
@doc.description("Add a new gameserver to kubernetes")
async def addServer(request):
    if not allowed_client(request.ip, ALLOWED_CLIENTS):
        raise ServerError("Client is not allowed")
    try:
        json_data = servers.addServer(request.ip, request.json)
        return json(json_data.get('data'), status=json_data.get('status'))
    except Exception as e:
        raise ServerError(e)


@app.delete("/api/servers/delete/<id>")
@doc.tag("servers")
@doc.description("Delete a gameserver from kubernetes")
async def deleteServer(request, id):
    if not allowed_client(request.ip, ALLOWED_CLIENTS):
        raise ServerError("Client is not allowed")
    try:
        json_data = servers.deleteServer(id, request.ip)
        return json(json_data.get('data'), status=json_data.get('status'))
    except Exception as e:
        raise ServerError(e)


@app.get("/api/games")
@doc.tag("games")
@doc.description("Get a list of games possible to deploy in kubernetes")
async def getGames(request):
    if not allowed_client(request.ip, ALLOWED_CLIENTS):
        raise ServerError("Client is not allowed")
    try:
        return json(games.getEnabledGames())
    except Exception as e:
        raise ServerError(e)


@app.get("/health")
@doc.exclude(True)
async def health():
    return text("Nice!")

#############################
###      DEV ROUTES       ###
#############################


@app.get("/getip")
@doc.exclude(True)
async def getip(request):
    return json({"request.ip": request.ip, "request.remote_addr": request.remote_addr})


@app.get("/tests/k8s/pod")
@doc.tag("k8s")
async def getPods(request):
    try:
        return text(k8s.pod(request.ip))
    except Exception as e:
        raise ServerError(e)


@app.get("/tests/k8s/deployment")
@doc.tag("k8s")
async def getDeployment(request):
    try:
        return text(k8s.deployment(request.ip))
    except Exception as e:
        raise ServerError(e)


@app.get("/tests/k8s/service")
@doc.tag("k8s")
async def getServices(request):
    try:
        return text(k8s.service(request.ip))
    except Exception as e:
        raise ServerError(e)

#############################
###       START APP       ###
#############################
if __name__ == "__main__":
    app.run(host="0.0.0.0", port=8000, auto_reload=True)
