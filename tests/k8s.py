from kubernetes import client
from sanic import request
import uuid
import ipaddress

def pod(user_ip):
    pods = client.CoreV1Api().list_pod_for_all_namespaces(
        watch=False,
        label_selector="app=gaas"
    )

    return pods

def deployment(user_ip):
    
    deployments = client.AppsV1Api().list_deployment_for_all_namespaces(
        watch=False,
        label_selector="app=gaas"
    )

    return deployments

def service(user_ip):

    services = client.CoreV1Api().list_service_for_all_namespaces(
        watch=False,
        label_selector="app=gaas"
    )

    return services