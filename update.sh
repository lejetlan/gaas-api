#!/bin/bash

./build_proto.sh
VERSION=$(date +%Y%m%d-%H%M%S)
docker build -t registry.npf.dk/gaas-api --no-cache .
docker tag registry.npf.dk/gaas-api registry.npf.dk/gaas-api:$VERSION

if [ $1 = "push" ]; then
  docker push registry.npf.dk/gaas-api
  docker push registry.npf.dk/gaas-api:$VERSION
fi
