from kubernetes import client
from api.games.defaults import (
    GameSpec,
    Param,
    ParamException,
    ParamTypes
)

class MinecraftGameSpec(GameSpec):

    id = "minecraft"
    name = "Minecraft Java edition"
    params = [Param(
        id="RCON_PASSWORD",
        type=ParamTypes.STRING,
        name="Server Admin Password",
        description="Password for the RCON server",
    ), Param(
        id="PVP",
        type=ParamTypes.STRING,
        name="PVP mode",
        description="Enable PVP mode, default disabled",
        optional=True,
        default="false"
    ), Param(
        id="SEED",
        type=ParamTypes.STRING,
        name="SEED",
        description="World seed",
        optional=True,
        default="npf"
    )]

    def get_param_constraints(self):
        return {
            "RCON_PASSWORD": [(lambda v: len(v) > 10, "RCON Password must be at least 10 characters.")],
            "PVP": [],
            "SEED": []
        }

    def make_deployment(self, params):
        env = [client.V1EnvVar(
                name=k,
                value=str(v)
        ) for k, v in params.items()]
        env.extend([client.V1EnvVar(name="EULA",
            value="TRUE"),
            client.V1EnvVar(name="MEMORY",
            value="4G"),
            client.V1EnvVar(name="ENABLE_RCON",
            value="TRUE")])
        return [client.V1Container(
            env=env,
            image="registry.npf.dk/minecraft:v1",
            name="minecraft",
            resources=client.V1ResourceRequirements(
                limits={
                    "cpu": "1",
                    "memory": "4G"
                }
            ),
            ports=[client.V1ContainerPort(
                container_port=25565,
                protocol="UDP"
            )]
        )]

    def make_service(self):
        return client.V1ServiceSpec(
            type="ClusterIP",
            cluster_ip="None"
        )
