import os
from kubernetes import client
from api.games.defaults import (
    GameSpec, 
    Param, 
    ParamException, 
    ParamTypes
)
from main import TokenHandler


class CSGoGameSpec(GameSpec):

    id = "csgo"
    name = "Counter Strike: Global Offensive"
    params = [Param(
        id="HOSTNAME",
        type=ParamTypes.STRING,
        name="Server Hostname",
        description="Shows up in server list",
    ), Param(
        id="SV_PASSWORD",
        type=ParamTypes.STRING,
        optional=True,
        name="Server Password",
        description="Password for the server",
    ), Param(
        id="RCON_PASSWORD",
        type=ParamTypes.STRING,
        name="Server Admin Password",
        description="Password for the RCON server",
    ), Param(
        id="MAP",
        type=ParamTypes.ARRAY,
        name="Map",
        description="Map the server will start on",
        elements=["de_cache", "de_cbble", "de_dust2", "de_inferno", "de_mirage", "de_nuke", "de_overpass", "de_train", "de_vertigo"]
    ), Param(
        id="SERVER_TYPE",
        type=ParamTypes.ARRAY,
        name="Server type",
        description="Choose server type for enabling different gamemodes",
        elements=["Deathmatch", "Pug", "Tournament-BareMetal", "Tournament-eBot", "Tournament-Get5"],
        default="Pug",
        optional=True,
    )]

    def get_param_constraints(self):
        return {
            "HOSTNAME": [(lambda v: len(v) > 5, "Steam server name needs to be more than 5 chars.")],
            "SV_PASSWORD": [],
            "RCON_PASSWORD": [(lambda v: len(v) > 10, "RCON Password must be at least 10 characters.")],
            "MAP": [],
            "SERVER_TYPE": []
        }

    def make_deployment(self, params, uid):
        env = [client.V1EnvVar(
                name=k,
                value=str(v)
        ) for k, v in params.items()]
        env.extend([client.V1EnvVar(
            name="GAME_TYPE",
            value="0"
        ), client.V1EnvVar(
            name="GAME_MODE",
            value="1"
        ), client.V1EnvVar(
            name="MAXPLAYERS",
            value="12"
        ), client.V1EnvVar(
            name="TICKRATE",
            value="128"
        ), client.V1EnvVar(
            name="SERVER_TOKEN",
            value=TokenHandler.generate(app_id=730, memo=uid)
        )])

        for e in env:
            if e.name == "HOSTNAME":
                e.value = "[GaaS] " + e.value + " | Provided by RUSH-B.DK"
            if e.name == "SERVER_TYPE":
                if e.value == "Tournament-eBot" or e.value == "Tournament-Get5" or e.value == "Tournament-BareMetal":
                    env.extend([client.V1EnvVar(
                        name="TOURNAMENT_SYSTEM",
                        value=e.value[11:]
                    )])
                    e.value = "Tournament"

        return [client.V1Container(
            env=env,
            image= "registry.npf.dk/csgo-server:v3",
            name="csgo",
            resources=client.V1ResourceRequirements(
                limits={
                    "cpu": "1",
                    "memory": "4G"
                }
            ),
            ports=[client.V1ContainerPort(
                container_port=27015,
                protocol="UDP"
            ), client.V1ContainerPort(
                container_port=27020,
                protocol="UDP"
            ), client.V1ContainerPort(
                container_port=27015,
                protocol="TCP"
            ), client.V1ContainerPort(
                container_port=27020,
                protocol="TCP"
            )]
        )]

    def make_service(self):
        return client.V1ServiceSpec(
                type="ClusterIP",
                cluster_ip="None",
                ports=[client.V1ServicePort(name="port1",
                                            port=27015,
                                            target_port=27015,
                                            protocol="UDP"),
                       client.V1ServicePort(name="port2",
                                            port=27020,
                                            target_port=27020,
                                            protocol="UDP"),
                       client.V1ServicePort(name="port3",
                                            port=27015,
                                            target_port=27015,
                                            protocol="TCP"),
                       client.V1ServicePort(name="port4",
                                            port=27020,
                                            target_port=27020,
                                            protocol="TCP")
                       ]
        )