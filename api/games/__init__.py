from api.games.specs.csgo import CSGoGameSpec
from api.games.specs.minecraft import MinecraftGameSpec

ENABLED_GAMES = {
    CSGoGameSpec.id: CSGoGameSpec().json(),
    MinecraftGameSpec.id: MinecraftGameSpec().json()
}

ENABLED_CLASSES = {
    CSGoGameSpec.id: CSGoGameSpec(),
    MinecraftGameSpec.id: MinecraftGameSpec()
}


def getEnabledGames():
    return ENABLED_GAMES


def getGameById(id):
    if id not in ENABLED_GAMES:
        raise Exception("GameID {} not found.".format(id))
    return ENABLED_CLASSES[id]
