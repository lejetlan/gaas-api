import ipaddress
import grpc
import uuid
from api.games import getGameById
from datetime import datetime, timedelta, timezone
from kubernetes import client
from sanic import request
from proto import token_pb2, token_pb2_grpc
from main import TokenHandler


def getServerList(user_ip):
    pods = client.CoreV1Api().list_pod_for_all_namespaces(
        watch=False,
        label_selector="app=gaas"
    )

    deployments = client.AppsV1Api().list_deployment_for_all_namespaces(
        watch=False,
        label_selector="app=gaas"
    )

    services = client.CoreV1Api().list_service_for_all_namespaces(
        watch=False,
        label_selector="app=gaas"
    )

    servers = {}

    for service in services.items:
        try:
            uid = service.metadata.labels["server"]

            if service.spec.external_i_ps is None:
                ip = service.spec.cluster_ip
            else:
                ip = service.spec.external_i_ps[0]

            servers[uid] = {
                "uid": service.metadata.labels["server"],
                "game": service.metadata.labels["game"],
                "service_ip": ip,
                "ports": [
                    "{}/{}".format(port.protocol, port.port)
                    for port in service.spec.ports
                ]
            }
        except KeyError as e:
            print(e)

    for deploy in deployments.items:
        try:
            uid = deploy.metadata.labels["server"]
            if uid not in servers:
                continue
            servers[uid]["namespace"] = deploy.metadata.namespace
            if deploy.metadata.labels["creator"] == user_ip:
                servers[uid]["candelete"] = "yes"
            for container in deploy.spec.template.spec.containers:
                if container.env:
                    servers[uid]["env"] = {
                        env.name: env.value
                        for env in container.env
                    }
        except KeyError as e:
            print(e)

    for pod in pods.items:
        try:
            uid = pod.metadata.labels["server"]
            if uid not in servers:
                continue
            if pod.status.container_statuses is None:
                continue
            servers[uid]["pod_ip"] = pod.status.pod_ip
            servers[uid]['run_time'] = formatTime(pod.status.start_time)
            servers[uid]["pods"] = [{
                "ready": status.ready,
                "image": status.image,
                "restart_count": status.restart_count,
                "state": status.state.waiting.reason if status.state.waiting is not None else None,
            } for status in pod.status.container_statuses]
        except (KeyError) as e:
            print(e)

    return servers


def getServerInfo(uid, user_ip):
    pods = client.CoreV1Api().list_pod_for_all_namespaces(
        watch=False,
        label_selector="app=gaas"
    )

    deployments = client.AppsV1Api().list_deployment_for_all_namespaces(
        watch=False,
        label_selector="app=gaas"
    )

    services = client.CoreV1Api().list_service_for_all_namespaces(
        watch=False,
        label_selector="app=gaas"
    )

    server = {}

    for service in services.items:
        try:
            if uid not in service.metadata.labels["server"]:
                continue
            if service.spec.external_i_ps is None:
                ip = service.spec.cluster_ip
            else:
                ip = service.spec.external_i_ps[0]
            server[uid] = {
                "uid":service.metadata.labels["server"],
                "game": service.metadata.labels["game"],
                "service_ip": ip,
                "ports": [
                    "{}/{}".format(port.protocol, port.port)
                    for port in service.spec.ports
                ]
            }
        except KeyError as e:
            print(e)

    for deploy in deployments.items:
        try:
            if uid not in server:
                continue
            server[uid]["namespace"] = deploy.metadata.namespace
            if deploy.metadata.labels["creator"] == user_ip:
                server[uid]["candelete"] = "yes"
            for container in deploy.spec.template.spec.containers:
                if container.env:
                    server[uid]["env"] = {
                        env.name: env.value
                        for env in container.env
                    }
        except KeyError as e:
            print(e)

    for pod in pods.items:
        try:
            if uid not in server:
                continue
            if pod.status.container_statuses is None:
                continue
            server[uid]["pod_ip"] = pod.status.pod_ip
            server[uid]['run_time'] = formatTime(pod.status.start_time)
            server[uid]["pods"] = [{
                "ready": status.ready,
                "image": status.image,
                "restart_count": status.restart_count,
                "state": status.state.waiting.reason if status.state.waiting is not None else None,
            } for status in pod.status.container_statuses]
        except (KeyError) as e:
            print(e)

    return server


def getServerNamespace(uid, user_ip):
    deployments = client.AppsV1Api().list_deployment_for_all_namespaces(
        watch=False,
        label_selector="app=gaas"
    )

    services = client.CoreV1Api().list_service_for_all_namespaces(
        watch=False,
        label_selector="app=gaas"
    )

    server = {}

    for service in services.items:
        try:
            if uid not in service.metadata.labels["server"]:
                continue
            server[uid] = {
                "uid": service.metadata.labels["server"],
                "game": service.metadata.labels["game"],
            }
        except KeyError as e:
            print(e)

    for deploy in deployments.items:
        try:
            if uid not in deploy.metadata.labels["server"]:
                continue
            server[uid]["namespace"] = deploy.metadata.namespace
            if deploy.metadata.labels["creator"] == user_ip:
                server[uid]["candelete"] = "yes"
        except KeyError as e:
            print(e)
    return server


def deleteServer(uid, user_ip):
    try:
        server = getServerNamespace(uid, user_ip)
        cluster_namespace = server[uid]["namespace"]
        
        deployment = client.AppsV1Api().read_namespaced_deployment_status(
            name="gaas-{}".format(uid),
            namespace=cluster_namespace,
        )
        
        if deployment.metadata.labels["creator"] != user_ip:
            return {"status":403, "data":{"message": "You did not create this server"}}
        else:
            client.AppsV1Api().delete_namespaced_deployment(
                name="gaas-{}".format(uid),
                namespace=cluster_namespace,
            )
            
            client.CoreV1Api().delete_namespaced_service(
                name="gaas-{}".format(uid),
                namespace=cluster_namespace,
            )
            print(deployment.spec.template.spec.containers)

            # Delete server token in steam
            if deployment.metadata.labels["game"] == "csgo":
                for env in deployment.spec.template.spec.containers[0].env:
                    if env.name == "SERVER_TOKEN":
                        TokenHandler.delete(env.value)
    except KeyError as e:
        print(e)

    return {"status": 200, "data": {"message": "Server have been deleted"}}

def addServer(user_ip, json_data):
    namespace = "gaas"

    server_type = json_data.get('params', {}).get('SERVER_TYPE')
    if server_type is not None:
        if server_type[0:10] == "Tournament":
            namespace = "tournament"

    id = json_data["id"]
    params = json_data["params"]

    game = getGameById(id)
    try:
        game.validate_params(params, game)
    except Exception as e:
        return {"status":404, "data":{"message": str(e)}}

    uid = uuid.uuid4().hex[:12]
    name = "gaas-{}".format(uid)
    labels = {
        "app": "gaas",
        "game": id,
        "server": uid,
        "creator": user_ip,
    }
    
    metadata=client.V1ObjectMeta(
        labels=labels,
        name=name,
    )

    extra_env=[client.V1EnvVar(
        name="IP_CREATOR",
        value=user_ip
    )]
    
    containers = game.make_deployment(params, uid)
    
    for container in containers:
        if container.env:
            container.env.extend(extra_env)
        else:
            container.env = extra_env
        if not container.resources:
            container.resources=client.V1ResourceRequirements(
                limits={
                    "cpu": "1",
                    "memory": "4G"
                }
            )
  
    if namespace == "tournament":
        affinity=client.V1Affinity(
            node_affinity=client.V1NodeAffinity(
                required_during_scheduling_ignored_during_execution=client.V1NodeSelector(
                    node_selector_terms=[
                        client.V1NodeSelectorTerm(
                            match_expressions=[
                                client.V1NodeSelectorRequirement(
                                    key="gaas-workertype",
                                    operator="In",
                                    values=["tournament"]
                                )
                            ]
                        )
                    ]
                )
            )
        )
    else:
        affinity = client.V1Affinity()

    deployment=client.V1Deployment(
            spec=client.V1DeploymentSpec(
                replicas=1,
                strategy=client.AppsV1beta1DeploymentStrategy(
                    rolling_update=client.AppsV1beta1RollingUpdateDeployment(
                        max_surge=0,
                        max_unavailable=1
                    )
                ),
                selector=client.V1LabelSelector(
                    match_labels=labels,
                ),
                template=client.V1PodTemplateSpec(
                    spec=client.V1PodSpec(
                        tolerations=[client.V1Toleration(
                            key="gaas",
                            effect="NoSchedule",
                            operator="Exists"
                        )],
                        containers=containers,
                        termination_grace_period_seconds=0,
                        affinity=affinity
                    )
                )
            )
    )

    service=client.V1Service(
        spec=game.make_service(params)
    )
    service.spec.selector = labels


    deployment.metadata = metadata
    deployment.spec.template.metadata = metadata
    service.metadata = metadata

    try:
       client.AppsV1Api().create_namespaced_deployment(
            namespace=namespace,
            body=deployment,
        )
    except Exception as e:
        print("**** STARTING TO PRINT ERROR MESSAGE ****")
        print(e)
        print("**** STOPPING TO PRINT ERROR MESSAGE ****")
        return {"status": 400, "data": {"message": "Error when creating pod"}}

    try:
        client.CoreV1Api().create_namespaced_service(
            namespace=namespace,
            body=service,
        )
    except Exception as e:
            print("**** STARTING TO PRINT ERROR MESSAGE ****")
            print(e)
            print("**** STOPPING TO PRINT ERROR MESSAGE ****")
            return {"status": 400, "data":{"message": "Error when creating pod"}}

    return {"status": 200, "data": {"message": {"uid": uid,
                                                "ip": user_ip,
                                                "hostname": "gaas-{}.gaas.svc.cluster.local".format(uid)
                                                }
                                    }
            }


def alloc_ip():
    external_prefix = "152.115.40.240/28"
    space = ipaddress.ip_network(external_prefix)
    reserved = []
    services = client.CoreV1Api().list_service_for_all_namespaces(watch=False)
    for service in services.items:
        if service.spec.external_i_ps:
            reserved.extend(service.spec.external_i_ps)
    for ip in space:
        if str(ip) not in reserved:
            return str(ip)
    raise Exception("Cluster ran out of available IPs")


def countServers(user_ip):
    deployments = client.AppsV1Api().list_deployment_for_all_namespaces(
        watch=False,
        label_selector="creator={}".format(user_ip)
    )
    if len(deployments.items) >= 3:
        return True


def formatTime(start):
    age = datetime.now(timezone.utc) - start
    hours, remainder = divmod(age.seconds, 3600)
    minutes, seconds = divmod(remainder, 60)
    if minutes > 0:
        if hours > 0:
            if hours == 1:
                return '{} hour and {} min'.format(int(hours), int(minutes))
            else: 
                return '{} hours and {} min'.format(int(hours), int(minutes))
        else:
            return '{} min and {} sec'.format(int(minutes), int(seconds))
    else:
        return '{} sec'.format(int(seconds))