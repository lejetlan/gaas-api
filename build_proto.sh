#!/bin/sh

# Get latest version of steam token proto
curl https://raw.githubusercontent.com/npflan/proto/main/steam-token/token.proto -o proto/token.proto -s

python -m grpc_tools.protoc -I.  --python_out=. --grpc_python_out=. proto/token.proto

rm proto/token.proto
