from proto import token_pb2, token_pb2_grpc
import grpc
import os


class SteamToken:
    def __init__(self):
        self.steam_token_service = "{}:{}".format(os.getenv('STEAM_TOKEN_HANDLER_URL', "steam-token-handler.gaas-backend.svc.cluster.local"),
                                                  os.getenv('STEAM_TOKEN_HANDLER_PORT', 8080))
        self.__connect()

    def __connect(self):
        self.grpc_channel = grpc.insecure_channel(self.steam_token_service)

    def __client(self):
        return token_pb2_grpc.TokenserviceStub(self.grpc_channel)

    def generate(self, app_id, memo):
        return self.__client().CreateToken(token_pb2.CreateTokenRequest(app_id=app_id, memo=memo)).server_token

    def delete(self, token):
        self.__client().DeleteToken(token_pb2.DeleteTokenRequest(server_token=token))