# GaaS-API

Gaming-as-a-Service API

### Run
Edit `docker-compose.yaml` and set environment variables for your given setup.

To start API locally run:

```docker-compose up```

### Settings

| Environment variables     | Description| Default |
|---------------------------| --- | --- |
| STEAM_WEB_API_KEY        	| The users API from steam, will be used for generating game token for servers. <br> Can be generate [here](https://steamcommunity.com/dev/apikey).  | None |
| STEAM_TOKEN_HANDLER_URL  	| URL where API can call TokenHandler   	| steam-token-handler.gaas-backend.svc.cluster.local   	|
| STEAM_TOKEN_HANDLER_PORT 	| PORT where API can call TokenHandler  	| 8080   	|


### Development
Docker compose will mount in local files and Sanic will update automatically.

### Build

Run `./update` to build a local docker image

Run `./update push` to build a local docker image and push it to the registry

##### Protofiles

To build new protofiles run `./build_proto.sh`

